core = 7.x
api = 2

; salesforce_webforms
projects[salesforce_webforms][type] = "module"
projects[salesforce_webforms][download][type] = "git"
projects[salesforce_webforms][download][url] = "https://git.drupal.org/project/salesforce_webforms.git"
projects[salesforce_webforms][download][tag] = "7.x-1.3"
projects[salesforce_webforms][subdir] = ""

; salesforce
projects[salesforce][type] = "module"
projects[salesforce][download][type] = "git"
projects[salesforce][download][url] = "https://git.drupal.org/project/salesforce.git"
projects[salesforce][download][tag] = "7.x-3.2"
projects[salesforce][subdir] = ""

; behavior_weights
projects[behavior_weights][type] = "module"
projects[behavior_weights][download][type] = "git"
projects[behavior_weights][download][url] = "https://git.drupal.org/project/behavior_weights.git"
projects[behavior_weights][download][tag] = "7.x-1.0"
projects[behavior_weights][subdir] = ""

; blockreference
projects[behavior_weights][type] = "module"
projects[behavior_weights][download][type] = "git"
projects[behavior_weights][download][url] = "https://git.drupal.org/project/blockreference.git"
projects[behavior_weights][download][tag] = "7.x-2.4"
projects[behavior_weights][subdir] = ""

; uw_salesforce_block
projects[uw_salesforce_block][type] = "module"
projects[uw_salesforce_block][download][type] = "git"
projects[uw_salesforce_block][download][url] = "https://git.uwaterloo.ca/mjsage/uw_salesforce_block.git"
projects[uw_salesforce_block][download][tag] = "7.x-1.1"
projects[uw_salesforce_block][subdir] = ""

; uw_ct_single_page_home
projects[uw_ct_single_page_home][type] = "module"
projects[uw_ct_single_page_home][download][type] = "git"
projects[uw_ct_single_page_home][download][url] = "https://git.uwaterloo.ca/mjsage/uw_ct_single_page_home.git"
projects[uw_ct_single_page_home][download][tag] = "7.x-1.14"
projects[uw_ct_single_page_home][subdir] = ""

; uw_ofis_eng
projects[uw_ofis_eng][type] = "module"
projects[uw_ofis_eng][download][type] = "git"
projects[uw_ofis_eng][download][url] = "https://git.uwaterloo.ca/mjsage/uw_ofis_eng.git"
projects[uw_ofis_eng][download][tag] = "7.x-1.6"
projects[uw_ofis_eng][subdir] = ""

; uw_theme_marketing
projects[uw_theme_marketing][type] = "theme"
projects[uw_theme_marketing][download][type] = "git"
projects[uw_theme_marketing][download][url] = "https://git.uwaterloo.ca/mjsage/uw_theme_marketing.git"
projects[uw_theme_marketing][download][tag] = "7.x-1.12"
projects[uw_theme_marketing][subdir] = ""
